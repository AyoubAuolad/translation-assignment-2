# Sign Language Translator

The assignment for module 2 was to develop a single page application with the REACT.js framework. For this assignment I designed a component tree with the Figma tool. The component tree is included in this repository.
The sign language translator consists of 3 views: the startup page, the translator page and the profile page.
In the translator view you can translate your input in letters to signlanguage images. All pages require authentication and you can view and clear your input whenever you want.

APP LINK: https://aqueous-falls-42365.herokuapp.com/
API LINK: https://ayoub-aa-noroff-api.herokuapp.com/translations

## TABLE OF CONTENTS
- Install
- Usage
- Maintainers
- Contributers
- License

## INSTALL
1. Clone or download a zip 
2. Install node modules -> npm install
3. Run -> npm start

## USAGE
- Login or register with a username and get redirected to the profile page.
- Navigate to translate.
- Fill in a text you want to translate to sign language.
- Sign language will appear in a box below.
- Your past 10 translations will appear in your profile page.
- You can clear your translation history on your profile page.
- You can logout in the navbar which only shows if you're logged in.

## MAINTAINERS 
@ayoub-auolad-ali

## CONTRIBUTERS
[link](https://github.com/RichardLitt/standard-readme)
<a href="https://github.com/RichardLitt/standard-readme">RICHARD TLS READ ME TEMPLATE</a>

## LICENSE
Copyright (c) 2012-2022 Scott Chacon and others

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
