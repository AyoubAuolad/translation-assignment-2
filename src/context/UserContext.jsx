import { createContext, useContext, useState } from "react";
import { STORAGE_KEY_USER } from "../const/storageKeys";
import { storageRead } from "../utils/storage";


//Context creation for user so we can use the state everywhere
const UserContext = createContext()

export const useUser = () => {
    return useContext(UserContext)
}

//provider
export const UserProvider = (props) => {

    //I set the user state to the current user in session
    const [user, setUser] = useState(storageRead(STORAGE_KEY_USER))

    const state = {
        user,
        setUser
    }

    return (
        <UserContext.Provider value={state}>
            {props.children}
        </UserContext.Provider>
    )
}