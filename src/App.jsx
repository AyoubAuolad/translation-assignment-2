import './App.css';
import {
  BrowserRouter,
  Routes,
  Route,
} from 'react-router-dom';
//import Startup from './views/Startup';
import Profile from './views/Profile';
import Translation from './views/Translation';
import StartupForm from './components/Startup/StartupForm';
import Navbar from './components/Navbar/Navbar';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {

  return (

    //Routing all pages
    <BrowserRouter>
  
      <div className="App">
        <Navbar/>

        <Routes>
          <Route path='/' element={<StartupForm />} />
          <Route path='/Profile' element={<Profile />} />
          <Route path='/Translation' element={<Translation />} />
        </Routes>
      </div>

      <footer></footer>
    </BrowserRouter>

  );
}

export default App;
