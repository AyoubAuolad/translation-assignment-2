import ProfileTranslationItem from "./ProfileTranslationItem"

const ProfileHistoryTranslations = ({ translations }) => {

    //Mapping all items into a list so we can display history of translations
    const translationList = translations.map(
        (translation, index) => <ProfileTranslationItem key={index + "-" + translation} translationItem={translation} />
    )

    return (
        <section className="section-past-translations">
            <h3>Your past translations:</h3>

            <ul>
                {translationList}
            </ul>
        </section>
    )
}

export default ProfileHistoryTranslations