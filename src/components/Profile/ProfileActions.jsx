import { Link } from "react-router-dom"
import { translationClearHistory } from "../../api/translation"
import { STORAGE_KEY_USER } from "../../const/storageKeys"
import { useUser } from "../../context/UserContext"
import { storageDelete, storageSave } from "../../utils/storage"
import Button from 'react-bootstrap/Button';



const ProfileActions = () => {

    //Get the user state 
    const { user, setUser } = useUser();

    //If user logs out session/local storage will be deleted and current user set to null
    const handleLogoutClick = () => {
        if (window.confirm("Are you sure?")) {
            storageDelete(STORAGE_KEY_USER)
            setUser(null)
        }
    }

    //Clear history
    const handleClearHistoryClick = async () => {
        if (!window.confirm("Are you sure?\nThis can not be undone.")) {
            return
        }

        //Translation are set to an empty array, if that didnt happen we get an error stored
        const [clearError] = await translationClearHistory(user.id)

        //If stored error is not empty, something went wrong while clearing history
        if (clearError !== null) {
            //Something went wrong
        }

        //Here we get the updated user object
        const updatedUser =
        {
            ...user,
            translations: []
        }

        //Here we save the updated user object in our state and local/session storage
        storageSave(STORAGE_KEY_USER, updatedUser)
        setUser(updatedUser)

    }

    return (

        //returning list of 2 buttons and 1 link element
        <section className="section-profile-actions">
        <ul>

            <li > <Link to="/translation" className="link-to-translations"> Start translating </Link> </li>
            <li> <Button variant="warning" className="text-white"  onClick={handleClearHistoryClick}> Clear Translation History </Button> </li>

        </ul>
        </section>
    )
}

export default ProfileActions