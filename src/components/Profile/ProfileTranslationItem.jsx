const ProfileTranslationItem = ({ translationItem }) => {

    //Here we get a single list item which is an image
    return (
        <li>{translationItem}</li>
    )
}
export default ProfileTranslationItem