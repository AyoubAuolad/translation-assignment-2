import TranslationImageItem from "./TranslationImageItem"

const TranslatedImages = ({ translations }) => {

    //All the translated images will be set to a list
    const translationList = translations.map(
        (translation, index) => <TranslationImageItem key={index + "-" + translation} translationItem={translation} />
    )
    return (
        <section className="section-translationlist">
                {translationList}
        </section>
    )
}

export default TranslatedImages