import { useForm } from "react-hook-form"
import Button from 'react-bootstrap/Button';


const TranslationNotes = ({ onTranslation }) => {

    //Get the register and submit handle from react hook form
    const { register, handleSubmit } = useForm()

    //If form is submitted we get the input inside translationNotes
    const onSubmit = ({ translationNotes }) => { onTranslation(translationNotes) }

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <div className="row">
                <div className="col-10">
                    <fieldset id="translation-input" >
                        <label htmlFor="translation-notes"></label>
                        <input type="text" {...register("translationNotes")} placeholder="What do you want to translate?" />
                    </fieldset>
                </div>
                    <div className="col-2">
                        <Button type="submit" className="btn-translate"> 🡪 </Button>
                    </div>
            </div>
        </form>
    )
}

export default TranslationNotes