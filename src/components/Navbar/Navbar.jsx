import { Button } from "react-bootstrap"
import { NavLink } from "react-router-dom"
import { STORAGE_KEY_USER } from "../../const/storageKeys"
import { useUser } from "../../context/UserContext"
import { storageDelete } from "../../utils/storage"




const Navbar = () => {

    const { user, setUser } = useUser()

    //If user logs out session/local storage will be deleted and current user set to null
    const handleLogoutClick = () => {
        if (window.confirm("Are you sure?")) {
            storageDelete(STORAGE_KEY_USER)
            setUser(null)
        }
    }

    return (
        <nav className="bg-dark py-4">
            <div className="container ">
                <div className="row">
                    <div className="col-6">
                    
                    <NavLink to="/"> <h3 className="text-light w-900" >Signlanguage Translations</h3> </NavLink>
                    
                    </div>


                    { user !== null &&

                        <div className="col-6">
                            <ul >
                                <li> <NavLink to="/Translation" className="text-light"> Translations </NavLink> </li>
                                <li> <NavLink to="/Profile" className="text-light"> Profile </NavLink> </li>
                                <li> <Button variant="danger" size="sm" onClick={handleLogoutClick}> Logout </Button> </li>

                            </ul>
                        </div>
                    }
                </div>
            </div>
        </nav>

    )
}

export default Navbar