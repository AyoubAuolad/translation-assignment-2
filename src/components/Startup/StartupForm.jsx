import { useForm } from "react-hook-form";
import { loginUser } from "../../api/user";
import { useState, useEffect } from "react";
import { storageSave } from "../../utils/storage";
import { useNavigate } from "react-router-dom";
import { useUser } from "../../context/UserContext";
import { STORAGE_KEY_USER } from "../../const/storageKeys";

import Button from 'react-bootstrap/Button';



const usernameConfig = {
    required: true,
    minLength: 2
}


const StartupForm = () => {
    //All Hooks
    const { register, handleSubmit, formState: { errors }
    } = useForm();
    const { user, setUser } = useUser() //useUser comes from UserContext
    const navigate = useNavigate()

    // All States
    const [loading, setLoading] = useState(false)
    const [apiError, setApiError] = useState(null)

    //Effect
    useEffect(() => {
        if (user !== null) {
            navigate('profile')
        }
        console.log("User has changed", user)
    }, [user, navigate]) //(empty)dependencies


    //If form gets submitted I login
    const onSubmit = async ({ username }) => {
        //While submitting I set the state of loading to true
        setLoading(true);

        const [error, userResponse] = await loginUser(username)

        if (error !== null) {
            setApiError(error)
        }

        if (userResponse !== null) {
            storageSave(STORAGE_KEY_USER, userResponse)
            setUser(userResponse);
        }

        setLoading(false);
    }

    //Here I declared the error message and specified on conditions
    const errorMessage = (() => {

        if (!errors.username) {
            return null
        }

        if (errors.username.type === 'required') {
            return <span>Username is required</span>
        }

        if (errors.username.type === 'minLength') {
            return <span>Username is too short</span>
        }

    })()


    return (

        <>
            <section className="container py-5">
                <form onSubmit={handleSubmit(onSubmit)}>

                    <div className="py-5">
                        <fieldset>
                            <label htmlFor="username"></label>
                            <input type="text"
                                {...register("username", usernameConfig)}
                                autoComplete="off"
                                required
                                placeholder="What's your name?"
                            />
                            {errorMessage}
                        </fieldset>
                    </div>

                    <section>
                        <Button variant="primary" type="submit" disabled={loading} >Continue</Button>
                        {loading && <p> Logging in... </p>}
                    </section>

                    {apiError && <p>{apiError}</p>}

                </form>
            </section>
        </>
    )
}

export default StartupForm