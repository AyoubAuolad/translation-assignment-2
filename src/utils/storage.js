//Save session if no error occur
export const storageSave = (key, value) => {

    if (!key || typeof key !== 'string') {
        throw new Error(' storageSave: Invalid key provided ')
    }

    if (!value) {
        throw new Error(' storageSave: No value provided for ' + key)
    }

    localStorage.setItem(key, JSON.stringify(value))
}

//Read from storage
export const storageRead = (key) => {
    const data = localStorage.getItem(key)
    if (data) {
        return JSON.parse(data)
    }

    return null
}

//Delete from storage
export const storageDelete = (key) => {
    localStorage.removeItem(key)
}