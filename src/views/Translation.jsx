import TranslationNotes from "../components/Translation/TranslationNotes"
import withAuth from "../hoc/withAuth"
import React, { useState } from "react"
import { useUser } from "../context/UserContext"
import { addTranslation } from "../api/translation"
import { storageSave } from "../utils/storage"
import { STORAGE_KEY_USER } from "../const/storageKeys"
import TranslatedImages from "../components/Translation/TranslatedImages"


//I declared an array of objects which consists of image specifications
const TRANSLATIONS = [
    {
        id: 1,
        name: "A",
        image: "img/a.png"
    },
    {
        id: 2,
        name: "B",
        image: "img/b.png"
    }
    ,
    {
        id: 3,
        name: "C",
        image: "img/c.png"
    }
    ,
    {
        id: 4,
        name: "D",
        image: "img/d.png"
    }
    ,
    {
        id: 5,
        name: "E",
        image: "img/e.png"
    },
    {
        id: 6,
        name: "F",
        image: "img/f.png"
    }
    ,
    {
        id: 7,
        name: "G",
        image: "img/g.png"
    }
    ,
    {
        id: 8,
        name: "H",
        image: "img/h.png"
    }
    , {
        id: 9,
        name: "I",
        image: "img/i.png"
    },
    {
        id: 10,
        name: "J",
        image: "img/j.png"
    }
    ,
    {
        id: 11,
        name: "K",
        image: "img/k.png"
    }
    ,
    {
        id: 12,
        name: "L",
        image: "img/l.png"
    }
    ,
    {
        id: 14,
        name: "M",
        image: "img/m.png"
    }
    ,
    {
        id: 15,
        name: "N",
        image: "img/n.png"
    }
    ,
    {
        id: 16,
        name: "O",
        image: "img/o.png"
    },
    {
        id: 17,
        name: "P",
        image: "img/p.png"
    },
    {
        id: 18,
        name: "Q",
        image: "img/q.png"
    }
    ,
    {
        id: 19,
        name: "R",
        image: "img/r.png"
    }
    ,
    {
        id: 20,
        name: "S",
        image: "img/s.png"
    }
    ,
    {
        id: 21,
        name: "T",
        image: "img/t.png"
    }
    ,
    {
        id: 22,
        name: "U",
        image: "img/u.png"
    }
    ,
    {
        id: 23,
        name: "W",
        image: "img/w.png"
    }
    ,
    {
        id: 24,
        name: "V",
        image: "img/v.png"
    }
    ,
    {
        id: 25,
        name: "X",
        image: "img/x.png"
    }
    ,
    {
        id: 26,
        name: "Y",
        image: "img/y.png"
    }
    ,
    {
        id: 27,
        name: "Z",
        image: "img/z.png"
    }
]

const Translation = () => {

    //2 states for the user and translated images
    const { user, setUser } = useUser()
    const [translatedImages, setTranslatedImages] = useState([])


    const handleTranslationClicked = async (translationNotes) => {

        //If nothing in input we give an alert and return null
        if (!translationNotes) {
            alert('Please provide an image before passing in your sentence')
            return null
        }

        //We get an array of each input character and set it to the state
        loopThroughArray(translationNotes)

        //HTTP Request to add the translation in our database
        const [error, updatedUser] = await addTranslation(user, translationNotes.trim())

        if (error !== null) {
            //something bad happened
            return
        }

        //syncing UI and SERVER state   
        storageSave(STORAGE_KEY_USER, updatedUser)
        //update the context state
        setUser(updatedUser)

        console.log("error translating: ", error)
        console.log("updatedUser translation: ", updatedUser)

    }



    //Get text that has to be translated as argument
    const loopThroughArray = (translationtext) => {

        //This array will consist of all the names of the images
        const arrayOfImageNames = []
        //The input for translation has been set to uppercase and no spaces and split it afterwards into an array
        const inputChars = translationtext.toUpperCase().replace(/ /g, '').split('')
        const imagesFromInput = []

        //Each object name of the images are getting pushed into arrayOfImageNames
        TRANSLATIONS.forEach(element => {
            arrayOfImageNames.push(element)
        })

        //Looping through all names in TRANSLATIONS object and find matching characters to each image object
        for (let i = 0; i < inputChars.length; i++) {
            var found = TRANSLATIONS.find(e => e.name === inputChars[i])
            //If matches found I push all values of the image property which consists of a string path
            imagesFromInput.push(found.image);
        }

        return (
            //The pushed image path will be set to the state 
            setTranslatedImages(imagesFromInput)
        )

    }


    return (
        <>

            <div className="text-center bg-dark">
            <section className="container py-5 ">
            <h1 className="text-light">Start translating to signlanguage</h1>

            <section id="translation-options" className="py-5 text-light">
                <TranslationNotes onTranslation={handleTranslationClicked} />
            </section>

            
            </section>
            
            </div>

            <section className="translated-images">
                <TranslatedImages translations={translatedImages} className="py-5" />
            </section>

        </>
    )
}

export default withAuth(Translation)