import StartupForm from "../components/Startup/StartupForm"

//This is my startup screen, you can login here
const Startup = () => {
    return (
        <>
        <section className="container">
            <h1>Login or register</h1>
           
            <StartupForm />
         </section>
        </>
    )
}

export default Startup