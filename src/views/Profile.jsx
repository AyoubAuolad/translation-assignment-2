//import { getUsernames } from "../api/translationsfetch";
import { useEffect } from "react"
import { userById } from "../api/user"
import ProfileActions from "../components/Profile/ProfileActions"
import ProfileHeader from "../components/Profile/ProfileHeader"
import ProfileHistoryTranslations from "../components/Profile/ProfileHistoryTranslations"
import { STORAGE_KEY_USER } from "../const/storageKeys"
import { useUser } from "../context/UserContext"
import withAuth from "../hoc/withAuth"
import { storageSave } from "../utils/storage"


function Profile() {

    const { user, setUser } = useUser() //Curly braces because user is an object

    useEffect(() => {
        //If not set to findUser const it wont logout
        const findUser = async () => {
            const [error, latestUser] = await userById(user.id)
            if (error === null) {
                storageSave(STORAGE_KEY_USER, latestUser)
                setUser(latestUser)
            }
        }
        //findUser()
    }, [setUser, user.id])

    return (
        <>
            <h1 className="text-center my-5">Welcome: {user.username} </h1>

            <ProfileHeader username={user.username} />
            <ProfileActions />
            <ProfileHistoryTranslations translations={user.translations} />
        </>
    )
}

export default withAuth(Profile) 