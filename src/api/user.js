import { createHeaders } from './index';

const apiUrl = process.env.REACT_APP_API_URL;

export const checkForUser = async (username) => {

    try {
        //here you get all users by changing path dynamically
        const response = await fetch(`${apiUrl}?username=${username}`)
        if (!response.ok) {
            throw new Error('Could not resolve your request.')
        }

        const data = await response.json()
        console.log(data)
        return [null, data]

    } catch (error) {
        return [error.message, []]
    }

}

const createUser = async (username) => {

    try {
        const response = await fetch(apiUrl, {
            method: 'POST',
            headers: createHeaders(),
            body: JSON.stringify({
                username,
                translations: []
            })
        })

        if (!response.ok) {
            throw new Error('Could not create an user with username: ' + username)
        }

        const data = await response.json()
        return [null, data]

    }
    catch (error) {
        return [error.message, []]
    }
}


export const loginUser = async (username) => {

    const [checkError, user] = await checkForUser(username);

    if (checkError !== null) {
        return [checkError, null]
    }

    if (user.length > 0) {
        return [null, user.pop()]
    }

    //create an user in return because the if statements break out if condition met
    return await createUser(username)

}

export const userById = async (userId) => {
    try {
        const response = await fetch(`${apiUrl}/${userId}`)

        if(!response.ok){
            throw new Error("Could not fetch single user")
        }

        const user = await response.json()
        return [null, user]

    } catch (error) {
        return [error.message, null]
    }
}