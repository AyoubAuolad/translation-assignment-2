import { createHeaders } from "."

const apiUrl = process.env.REACT_APP_API_URL;

export const addTranslation = async (user, translation) => {
    try {

        //Patching the current translations array
        const response = await fetch(`${apiUrl}/${user.id}`, {
            method: 'PATCH',
            headers: createHeaders(),
            body: JSON.stringify({
                translations: [...user.translations, translation]
            })
        })

        if (!response.ok) {
            throw new Error("Could not update translations")
        }

        const result = await response.json()

        //If translations array is 10 -> shift the first translation
        if (result.translations.length === 11) {
            result.translations.shift()
        }
        //Return nothing or the array as result
        return [null, result]

    }
    // Return an error message or return nothing
    catch (error) {
        return [error.message, null]
    }
}


export const translationClearHistory = async (userId) => {
    try {

        //Posting an empty array of translations back .
        const response = await fetch(`${apiUrl}/${userId}`, {
            method: 'PATCH',
            headers: createHeaders(),
            body: JSON.stringify({
                translations: []
            })
        })

        if (!response.ok) {
            throw new Error('Could not clear the translations')
        }

        const result = await response.json()

        return [null, result]

    } catch (error) {
        return [error.message, null]
    }
}
