
const apiKey = process.env.REACT_APP_API_KEY

export const createHeaders = () => {
    //Creating headers for api key and type of format
    return {
        'Content-Type': 'application/json',
        'x-api-key': apiKey
    }
}

